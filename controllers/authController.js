/* eslint-disable linebreak-style */
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {User} = require('../models/User');
const {validateRegistration} = require('../helpers/validateRegistration');
const nodemailer = require('nodemailer');
const passwordGenerator = require('generate-password');

const MAIL_ADRESS = 't3stforhw@gmail.com';
const MAIL_PASS = 'bbuoupew';


const registerUser = async (req, res) => {
  const validationResult = await validateRegistration.validate(req.body);

  if (validationResult.error) {
    return res.status(400)
        .send({'message': `${validationResult.error.message}`});
  }

  const {email, password: userPassword, role} = req.body;
  const existedUser = await User.findOne({email});

  if (existedUser) {
    return res.status(400)
        .send({'message': `User with  are already exists`});
  };

  const password = bcrypt.hashSync(userPassword, 5);

  try {
    const user = new User({
      email,
      password,
      role,
      createdDate: new Date(Date.now()),
    });

    await user.save();

    return res.status(200).send({'message': 'User has been saved'});
  } catch (error) {
    return res.status(500).send({'message': `${error}`});
  }
};

const loginUser = async (req, res) => {
  const {email, password} = req.body;
  const existedUser = await User.findOne({email});
  if (!email || !password) {
    return res.status(400)
        .send({'message': `username or password field are empty`});
  }
  if (!existedUser) {
    return res.status(400)
        .send({'message': `Username not found`});
  }
  if (!await bcrypt.compare(password, existedUser.password)) {
    return res.status(400).send({'message': 'Wrong password'});
  };
  // eslint-disable-next-line camelcase
  const token = jwt.sign(
      {_id: existedUser._id,
        role: existedUser.role},
      process.env.JWT_SECRET);
  return res.status(200).json({message: 'Success', jwt_token: token});
};

const forgotPassword = async (req, res) => {
  try {
    const {email} = req.body;
    if (!email) {
      return res.status(400).send({'message': 'Email filed is empty'});
    }
    const existedUser = await User.findOne({email});
    if (!existedUser) {
      return res.status(400)
          .send({'message': `Username not found`});
    };
    const generatedPassword = passwordGenerator.generate({
      length: 10,
      numbers: true,
    });
    const password = bcrypt.hashSync(generatedPassword, 5);
    await existedUser.updateOne({password});
    await existedUser.save();
    const sendLetter = async (receiver, newPass) => {
      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: MAIL_ADRESS,
          pass: MAIL_PASS,
        },
      });

      const message = {
        from: 'Test <zubkov.artem46@gmail.com>',
        to: receiver,
        subject: 'New password',
        html: `<p>Your new password: ${newPass}</p>`,
      };
      await transporter.sendMail(message);
    };
    await sendLetter(existedUser.email, generatedPassword);
    return res.status(200)
        .send({'message': 'New password sent to your email address'});
  } catch (e) {
    return res.status(500).send({'message': 'Server error'});
  };
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
};
