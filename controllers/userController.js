/* eslint-disable linebreak-style */
const {User} = require('../models/User');

const getUserInfo = async (req, res) => {
  const _id = req.userInfo._id;
  const user = await User.findById(_id);
  const {email, createdDate} = user;
  res.status(200).json({
    user: {
      _id,
      email,
      createdDate,
    },
  });
};
const deleteUserProfile = async (req, res) => {
  const _id = req.userInfo._id;
  const role = req.userInfo.role;
  if (role === 'DRIVER') {
    return res.status(400).json({message: 'Driver can not delete account'});
  };
  await User.findByIdAndDelete(_id);
  await Note.deleteMany({userId: _id});
  res.status(200).json({message: 'User has been deleted.'});
};
const changePassword = async (req, res) => {
  const _id = req.userInfo._id;
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(_id);
  const isValidPassword = await bcrypt.compare(oldPassword, user.password);

  if (!isValidPassword) {
    return res.status(400).json({message: 'Invalid old password.'});
  }

  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();
  res.status(200).json({message: 'Password has been changed.'});
};

module.exports = {
  getUserInfo,
  deleteUserProfile,
  changePassword,
};
