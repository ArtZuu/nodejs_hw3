/* eslint-disable linebreak-style */
const {Truck} = require('../models/Truck');
const {validateTruck} = require('../helpers/validateTruck');
const defineDimension = require('../helpers/defineDimension');

const getTrucks = async (req, res) => {
  const {_id} = req.userInfo;
  const trucks = await Truck.find({created_by: _id}, {__v: 0});
  if (trucks.length === 0) {
    return res.status(200).json({trucks: []});
  };
  return res.status(200).json({trucks});
};
const addTruck = async (req, res) => {
  const validationResult = await validateTruck.validate(req.body);

  if (validationResult.error) {
    return res.status(400)
        .send({'message': `${validationResult.error.message}`});
  };
  const {_id} = req.userInfo;
  const {type} = req.body;

  const truck = new Truck({
    type,
    created_by: _id,
    dimension: defineDimension(type),
  });

  await truck.save();

  return res.status(200).json({message: 'Truck created successfully'});
};
const getTruckById = async (req, res) => {
  const {id} = req.params;
  const getById = await Truck.findOne({_id: id}, {__v: 0});
  return res.status(200).send(getById);
};
const updateTruckById = async (req, res) => {
  const {type} = req.body;
  const {id} = req.params;
  const truck = await Truck.findOne({_id: id}, {__v: 0});
  truck.type = type;
  truck.dimension = defineDimension(type);
  truck.save();
  return res.status(200).json({message: 'Truck details changed successfully'});
};
const deleteTruckById = async (req, res) => {
  const {id} = req.params;
  await Truck.deleteOne({_id: id});
  return res.status(200).json({message: 'Truck deleted successfully'});
};

const assignTruckById = async (req, res) => {
  const {id} = req.params;
  try {
    await Truck.findByIdAndUpdate(id, {assigned_to: req.userInfo._id});
    res.status(200).json({message: 'Truck details changed successfully'});
  } catch {
    return res.status(404).json({
      message: `A truck with id '${id}' was not found`,
    });
  }
};

module.exports = {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
