/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
const {Load} = require('../models/Load');
const {Truck} = require('../models/Truck');

const getLoads = async (req, res) => {
  const offset = parseInt(req.query.offset) || 0;
  let limit = parseInt(req.query.limit) || 10;
  limit = limit > 50 ? 50 : limit;

  if (req.userInfo.role === 'DRIVER') {
    const loads = await Load.find(
        {assigned_to: req.user._id, status: req.query.status},
        [],
        {
          skip: offset,
          limit: limit,
        });
    return res.status(200).json({loads});
  } else if (req.userInfo.role === 'SHIPPER') {
    const loads = await Load.find(
        {created_by: req.userInfo._id, status: req.query.status},
        [],
        {
          skip: offset,
          limit: limit,
        },
    );
    return res.status(200).json({loads});
  };
};

const addLoad = async (req, res) => {
  const {name, payload, pickup_address,
    delivery_address, dimensions} = req.body;
  const {_id} = req.userInfo;

  const load = new Load({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_by: _id,
  });

  await load.save();

  return res.status(200).json({message: 'Load created successfully'});
};
const getActiveLoads = async (req, res) => {
  return res.status(200).json({load: req.activeLoad});
};
const updateState = async (req, res) => {
  await Load.updateOne(req.load, req.body);
  return res.status(200).json({message: 'Load details changed successfully'});
};
const getLoadById = async (req, res) => {
  const {id} = req.params;
  const load = await Load.findOne({_id: id}, {__v: 0});
  return res.status(200).json(load);
};
const deleteLoadById = async (req, res) => {
  const {id} = req.params;
  await Load.deleteOne({_id: id}, {__v: 0});
  return res.status(200).json({message: 'Load deleted successfully'});
};
const updateLoadById = async (req, res) => {
  const loadId = req.params.id;
  const {
    name,
    payload,
    pickup_address: pickup,
    delivery_address: delivery,
    dimensions,
  } = req.body;

  await Load.findByIdAndUpdate(
      {_id: loadId},
      {
        name,
        payload,
        pickup_address: pickup,
        delivery_address: delivery,
        dimensions,
      },
  );

  res.json({message: 'Load details changed successfully'});
};
const postLoadById = async (req, res) => {
  const id = req.params.id;
  const load = await Load.findById(id);

  if (load) {
    const {dimensions: {height, width, length}, payload} = load;

    const matchedTruck = await Truck.findOne()
        .where('status').equals('IS')
        .where('payload').gt(payload)
        .where('dimensions.height').gt(height)
        .where('dimensions.width').gt(width)
        .where('dimensions.length').gt(length);

    if (matchedTruck) {
      matchedTruck.status = 'OL';
      load.status = 'ASSIGNED';
      load.assigned_to = matchedTruck.created_by;
      load.state = 'En route to Pick Up';
      load.truck = matchedTruck._id;
      load.logs = [
        ...load.logs,
        {
          message: `Load assigned to driver with id ${matchedTruck.created_by}`,
        },
      ];

      await matchedTruck.save();
      await load.save();
      res.status(200)
          .json({message: 'Load posted successfully', driver_found: true});
    } else {
      load.logs = [...load.logs, {message: `Could't not find driver`}];
      await load.save();
      res.status(400);
      throw new Error('No Available Driver Found');
    }
  } else {
    res.status(400);
    throw new Error('No Load Found');
  };
};
const getShippingInfo = async (req, res) => {
  await isShipper(req, res);
  const loadId = req.params.id;

  const load = await Load.findOne({_id: loadId}, {__v: 0});
  const truck = await Truck.findOne({assigned_to: id}, {__v: 0});
  return res.status(200).json({load, truck});
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoads,
  updateState,
  getLoadById,
  deleteLoadById,
  updateLoadById,
  postLoadById,
  getShippingInfo,
};
