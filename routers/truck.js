/* eslint-disable linebreak-style */
const express = require('express');
const router = new express.Router();
const {getTrucks, addTruck, getTruckById,
  updateTruckById, deleteTruckById,
  assignTruckById} = require('../controllers/truckController');

router.get('/', getTrucks);
router.post('/', addTruck);
router.get('/:id', getTruckById);
router.put('/:id', updateTruckById);
router.delete('/:id', deleteTruckById);
router.get('/:id/assign', assignTruckById);

module.exports = router;
