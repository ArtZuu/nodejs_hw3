/* eslint-disable linebreak-style */
const express = require('express');
const router = new express.Router();
const {getUserInfo, deleteUserProfile,
  changePassword} = require('../controllers/userController');


router.get('/me', getUserInfo);

router.post('/me', deleteUserProfile);

router.patch('/me/password', changePassword);

module.exports = router;
