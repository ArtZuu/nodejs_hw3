/* eslint-disable linebreak-style */
const express = require('express');
const router = new express.Router();
const {getLoads, addLoad, getActiveLoads,
  updateState, getLoadById, deleteLoadById, updateLoadById,
  postLoadById, getShippingInfo} = require('../controllers/loadController');

router.get('/', getLoads);
router.post('/', addLoad);
router.get('/active', getActiveLoads);
router.patch('/active/state', updateState);
router.get('/:id', getLoadById);
router.put('/:id', updateLoadById);
router.delete('/:id', deleteLoadById);
router.post('/:id/post', postLoadById);
router.get('/:id/shipping_info', getShippingInfo);

module.exports = router;
