/* eslint-disable linebreak-style */
const express = require('express');
const router = new express.Router();
const {loginUser, registerUser,
  forgotPassword} = require('../controllers/authController');

router.post('/login', loginUser);

router.post('/register', registerUser);

router.post('/forgot_password', forgotPassword);

module.exports = router;
