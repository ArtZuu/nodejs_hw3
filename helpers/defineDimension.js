/* eslint-disable linebreak-style */
const SPRINTER = {
  'SPRINTER': {
    'volume': 12750000,
    'payload': 1700,
  },
};

const SMALL_STRAIGHT = {
  'SMALL_STRAIGHT': {
    'volume': 21250000,
    'payload': 2500,
  },
};

const LARGE_STRAIGHT = {
  'LARGE_STRAIGHT': {
    'volume': 49000000,
    'payload': 4000,
  },
};

const defineDimension = (typeTruck) => {
  switch (typeTruck) {
    case 'SPRINTER':
      return SPRINTER;
    case 'LARGE STRAIGHT':
      return LARGE_STRAIGHT;
    case 'SMALL STRAIGHT':
      return SMALL_STRAIGHT;
    default: return {volume: undefined, payload: undefined};
  }
};

module.exports = defineDimension;
