/* eslint-disable linebreak-style */
const Joi = require('joi');

const validateTruck = Joi.object({
  type: Joi.string().email()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT').required(),
});

module.exports = {
  validateTruck,
};
