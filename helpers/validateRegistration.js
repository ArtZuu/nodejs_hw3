/* eslint-disable linebreak-style */
const Joi = require('joi');

const validateRegistration = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(5).max(50).required(),
  role: Joi.string().valid('SHIPPER', 'DRIVER').required(),
});

module.exports = {
  validateRegistration,
};
