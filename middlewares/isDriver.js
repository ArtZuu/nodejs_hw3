/* eslint-disable linebreak-style */
const isDriver = () => {
  return (req, res, next) => {
    if (req.userInfo.role !== 'DRIVER') {
      return res.status(400).json({message: 'Access denied'});
    } else {
      next();
    }
  };
};

module.exports = isDriver;
