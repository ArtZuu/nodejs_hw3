/* eslint-disable linebreak-style */
const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },

  assigned_to: {
    type: String,
    default: 'NA',
  },

  type: {
    type: String,
    required: true,
    uppercase: true,
  },

  status: {
    type: String,
    default: 'IS',
  },

  dimensions: {
    type: Object,
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },
},
{
});

const Truck = mongoose.model('Truck', truckSchema);
module.exports = {
  Truck,
};
