/* eslint-disable linebreak-style */
const mongoose = require('mongoose');
const loadSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    default: 'Load has been created',
  },
  assigned_to: {
    type: String,
    default: 'NA',
  },
  status: {
    type: String,
    default: 'NEW',
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },

  },
  logs: {
    type: Array,
    default: [
      {
        message: 'Init message',
        time: new Date(Date.now()),
      },
    ],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
})
;
const Load = mongoose.model('Load', loadSchema);
module.exports = {
  Load,
};
